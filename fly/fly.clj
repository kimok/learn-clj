(ns fly.fly
  (:require [odoyle.rules :as o]))

(defn instrument [rules]
  (->> rules
       (map (fn [rule]
              (o/wrap-rule rule
                           {:what
                            (fn [f session new-fact old-fact]
                              (println :what (:name rule) new-fact old-fact)
                              (f session new-fact old-fact))
                            :when
                            (fn [f session match]
                              (println :when (:name rule) match)
                              (f session match))
                            :then
                            (fn [f session match]
                              (println :then (:name rule) match)
                              (f session match))
                            :then-finally
                            (fn [f session]
                              (println :then-finally (:name rule))
                              (f session))})))))

(defn died? [was-alive is-alive]
  (print was-alive " " is-alive)
  (and was-alive
       (not is-alive)))

(def rules
  (o/ruleset
   {::altitude [:what [::player ::altitude a]
                :then (println "You fly to an height of:" a)]
    ::fall     [:what
                [::time ::delta dt]
                [::player ::altitude pa {:then false}]
                [::ground ::altitude ga]
                :when (> pa ga)
                :then (o/insert! ::player ::altitude (- pa dt))]
    ::flap     [:what
                [::player ::action ::flap!]
                [::player ::altitude pa {:then false}]
                [::player ::alive? true]
                :then
                (o/insert! ::player ::altitude (+ pa 5))]
    ::crash    [:what
                [::player ::alive? true]
                [::player ::altitude pa]
                [::ground ::altitude ga]
                :when (<= pa ga)
                :then
                (println "You crash!")
                (o/insert! ::player ::alive? false)]
    ::die      [:what [::player ::alive? false {:then not=}]
                :then (println "You die!")]}))

(defn make-session [] (reduce o/add-rule (o/->session) rules))

(def session (atom (make-session)))

(defn setup []
  (swap! session #(-> %
                      (o/insert ::player ::alive? true)
                      (o/insert ::ground ::altitude 0)
                      (o/insert ::player ::altitude 20)
                      o/fire-rules))
  (println "Game start!"))

(defn flap []
  (swap! session #(-> %
                      (o/insert ::player ::action ::flap!)
                      o/fire-rules))
  nil)

(defn tick []
  (swap! session #(-> %
                      (o/insert ::time ::delta 5)
                      o/fire-rules))
  nil)

(def session
  (->> rules
       #_instrument
       (reduce o/add-rule (o/->session))
       atom))

(setup)
(flap)
(tick)
