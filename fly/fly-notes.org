
* fly
:PROPERTIES:
:org-remark-file: ~/learn-clj/fly/fly.clj
:END:

** :what
:PROPERTIES:
:org-remark-beg: 1123
:org-remark-end: 1128
:org-remark-id: 69d3ec77
:org-remark-label: blue
:org-remark-link: [[file:~/learn-clj/fly/fly.clj::32]]
:END:
Add tuples after :what. They look like:

  [:id :attribute :value]

If there's a symbol, O'Doyle binds it.
For example:

  [:apple :color c]
  
If the fact [:apple :color "red"]
is in the session,
O'Doyle binds c to "red.

You can use these bindings
in other parts of the rule.

** {:then false}
:PROPERTIES:
:org-remark-beg: 1313
:org-remark-end: 1326
:org-remark-id: 79d1d08a
:org-remark-label: blue
:org-remark-link: [[file:~/learn-clj/fly/fly.clj::36]]
:END:
You can put an optional map
at the end of a tuple.

{:then false} tells O'Doyle
not to evaluate [[:then]]
if only this tuple matches.

If another tuple matches,
O'Doyle still evaluates [[:then]].
** :when
:PROPERTIES:
:org-remark-beg: 1385
:org-remark-end: 1390
:org-remark-id: 807fbbe1
:org-remark-label: blue
:org-remark-link: [[file:~/learn-clj/fly/fly.clj::38]]
:END:
When a rule with a :when matches,
O'Doyle evaluates the expression.
If it evaluates to true,
O'Doyle does [[:then]].
If it's false, O'Doyle does nothing.

** (< y ground-y)
:PROPERTIES:
:org-remark-beg: 1391
:org-remark-end: 1400
:org-remark-id: fca05d2d
:org-remark-label: red
:org-remark-link: [[file:~/learn-clj/fly/fly.clj::38]]
:END:
The player should only fall
when they're above ground.

In game programming, it's common
for the origin to be at the top-left
of the window.

origin X-----------| (0, 0)
       |           |  
player |  X        | (20, 20)
ground |^^^^^^^^^^^| (_,  30)
       |-----------|

** :then
:PROPERTIES:
:org-remark-beg: 1417
:org-remark-end: 1422
:org-remark-id: 046b17cf
:org-remark-label: red
:org-remark-link: [[file:~/learn-clj/fly/fly.clj::39]]
:END:
A rule matches when
one of its [[:what]] tuples
matches a fact in the session.

You can put expressions after :then.
O'Doyle will evaluate them when the rule matches.

** insert!
:PROPERTIES:
:org-remark-beg: 1426
:org-remark-end: 1433
:org-remark-id: 2d4077bb
:org-remark-label: green
:org-remark-link: [[file:~/learn-clj/fly/fly.clj::39]]
:END:
You can call insert! in a rule.
O'Doyle adds a fact to the session.

This call adds to the player height (y)
based on how much time has passed (dt).
That's our idea of falling!

** reduce
:PROPERTIES:
:org-remark-beg: 2132
:org-remark-end: 2138
:org-remark-id: 26ab2c7f
:org-remark-label: blue
:org-remark-link: [[file:~/learn-clj/fly/fly.clj::57]]
:END:
You can process a collection with ~reduce~.
(reduce FN VAL COLL)

The FN should take two arguments:
It should return a new version of VAL.
(fn VAL ITEM) -> NEW-VAL

~reduce~ does:
- (FN VAL (first COLL)) -> NEW-VAL
  Use the first item of COLL on VAL.
- (FN NEW-VAL (second COLL)) -> NEW-NEW-VAL
  Use the second item on the new VAL.
- Keep going to the end of COLL.

** add-rule
:PROPERTIES:
:org-remark-beg: 2141
:org-remark-end: 2149
:org-remark-id: eb212ce8
:org-remark-label: green
:org-remark-link: [[file:~/learn-clj/fly/fly.clj::57]]
:END:

In this case, we want to add each rule to the session.

~reduce~ is the perfect way to do this!

~add-rule~ takes a session and a rule, and returns a new session.

(add-rule SESSION RULE) -> NEW-SESSION
** ->session
:PROPERTIES:
:org-remark-beg: 2153
:org-remark-end: 2162
:org-remark-id: 5ef55876
:org-remark-label: red
:org-remark-link: [[file:~/learn-clj/fly/fly.clj::57]]
:END:
This just returns a blank session.

We ~reduce~ it to add all the rules.

** atom
:PROPERTIES:
:org-remark-beg: 2187
:org-remark-end: 2191
:org-remark-id: 8acaadec
:org-remark-label: blue
:org-remark-link: [[file:~/learn-clj/fly/fly.clj::59]]
:END:
We use ~atom~ to identify something that can change value.

** reset!
~reset!~ changes the value of an atom.
In this case, the atom named ~session~.

(reset! ATOM NEW-VAL)

After you call ~reset!~, the atom will have a new value.

** swap!
:PROPERTIES:
:org-remark-beg: 2640
:org-remark-end: 2645
:org-remark-id: fb8fba9b
:org-remark-label: red
:org-remark-link: [[file:~/learn-clj/fly/fly.clj::76]]
:END:
~swap!~ changes the value of an atom.
In this case, the atom named ~session~.
It applies a function to the atom's value.
Then it evaluates the function for the atom's new value.

(swap! ATOM FN)

** insert
This adds a new fact to the session.
(insert SESSION ID ATTRIBUTE VALUE) -> NEW-SESSION

** fire-rules
This is the part that runs the rules-engine.
Everything we defined in ~rules~ will take effect with the new session.


** {:then (fn [a b] (not= a b))}
:PROPERTIES:
:org-remark-beg: 2047
:org-remark-end: 2059
:org-remark-id: 3cb6d0ee
:org-remark-label: blue
:org-remark-link: [[file:~/learn-clj/fly/fly.clj::54]]
:END:
You can use any function for :then in a tuple:
(fn VAL NEW-VAL) -> true/false

O'Doyle only does `:then` if the function evals true.

** died?
:PROPERTIES:
:org-remark-beg: 981
:org-remark-end: 986
:org-remark-id: 7c5f7b25
:org-remark-label: green
:org-remark-link: [[file:~/learn-clj/fly/fly.clj::25]]
:END:
Only alive people can die:
(died? ALIVE DEAD) -> true

If you're still alive, you didn't die:
(died? ALIVE ALIVE) -> false

If you're already dead, you didn't die:
(died? DEAD DEAD) -> false

If you go from dead to alive, you're a zombie:
(died? DEAD ALIVE) -> false
