* Getting Started
** Kimacs
Kimacs is an editor designed for use with this curriculum.
Kimacs is a configuration of emacs.

*** Install emacs

On Linux (ubuntu):
#+begin_src bash
sudo apt install emacs
#+end_src

On MacOS (with [[brew.sh][homebrew]]):
#+begin_src bash
brew install --cask emacs
#+end_src

*** Install git

Linux:
#+begin_src bash
sudo apt install git
#+end_src

MacOS:
#+begin_src bash
brew install git
#+end_src

*** Install kimacs
#+begin_src bash
git clone https://gitlab.com/kimok/kimacs.git ~/.config/emacs
#+end_src
Note: this won't work if you already have an emacs config.
You may need to delete an existing config before kimacs will load.
You can also use a profile manager like [[https://github.com/plexus/chemacs2][chemacs]] to keep both configs.

** Development tools
This course focuses on the programming language Clojure.

Some tools are required:

- git :: version control system.
  use it to download and work on this codebase.
- npm :: node package manager.
  use it to install more tools.
- java SDK :: tooling for java.
  shadow-cljs requires it.
- node.js :: javascript runtime.
  clojure and shadow-cljs require it.
- clojure :: clojure tooling.
- shadow-cljs :: tooling for clojure's javascript flavor.
  use npm to install it.

- Install package managers

MacOS:
#+begin_src bash
brew install git npm
#+end_src

Linux:
#+begin_src bash
sudo apt install git npm
#+end_src

*** Install runtimes

MacOS:
#+begin_src bash
brew install node adoptopenjdk
#+end_src

Linux:
#+begin_src bash
sudo apt install nodejs openjdk-11-jre
#+end_src

*** Install clojure tools

MacOS:
#+begin_src bash
brew install clojure shadow-cljs
#+end_src

Linux:
#+begin_src bash
sudo apt install clojure shadow-cljs
#+end_src

** Collaboration tools

*** Tuntox (experimental)

[[https://github.com/gjedeer/tuntox][tuntox]] and [[https://code.librehq.com/qhong/crdt.el/][crdt.el]] make collaborative editing possible.

[[https://nixos.org/][Nix]] provides a simple way to install tuntox.

With [[https://nixos.org/download.html][nix installed]]:
#+begin_src bash
nix-env -iA nixpkgs.tuntox
#+end_src

Note: Nix can use a lot of system resources.
Tuntox requires [[https://github.com/gjedeer/tuntox#security--threat-model][extra caution]] to run securely.
These collaboration tools are optional.
Please don't run them on machines with access to sensitive data or networks.
