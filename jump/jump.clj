(ns jump
  (:require [clojure.core.async :refer [<! timeout go-loop]]
            [tick.core :as time]
            [membrane.ui   :as ui]
            [membrane.skia :as skia]
            [membrane.java2d :as java2d]
            [membrane.components.code-editor.code-editor :as code]))

(def state (atom {:frame-number   0
                  :last-time      (time/now)
                  :ground-level   500
                  :world-width    500
                  :sky-height     500
                  :ground-depth   200
                  :player-y       0
                  :player-x       0
                  :player-y-speed 0
                  :player-x-speed 0
                  :mouse-position [0 0]}))

(def blue [53/100 81/100 92/100])
(def green [0 0.5 0])
(def brown [55/100 27/100 7/100])
(def red [1 0 0])

(defn run [direction]
  (swap! state assoc :player-x-speed (case direction
                                       :left -10 :right 10 0)))

(defn sky [width height]
  (ui/with-color blue
    (ui/rectangle width height)))

(defn trunk [width height]
  (ui/with-color brown
    (ui/rectangle width height)))

(defn canopy [width height]
  (ui/with-color green
    (ui/rectangle width height)))

(defn tree [width height]
  (ui/vertical-layout
   (ui/center (canopy (* 4 width) (/ height 2)) [width height])
   (trunk width (/ height 2))))

(defn trees [{:keys [ground-level]}]
  (let [[width height] [40 600]]
    (ui/translate
     50 (- ground-level height)
     (apply ui/horizontal-layout
            (take 24 (cycle [(tree width height)
                             (sky (* 3 width) height)]))))))

(defn jump []
  (swap! state assoc :player-y-speed -30))

(defn player []
  (->> (ui/with-color green
         (ui/rounded-rectangle 50 50 20))
       (ui/on :key-press (fn [key]
                           (case key
                             :up    (jump)
                             :left  (run :left)
                             :right (run :right)
                             nil)))))

(defn ground [{:keys [ground-level world-width ground-depth]}]
  (ui/translate
   0 ground-level
   [(ui/with-color brown
      (ui/rectangle world-width ground-depth))
    (ui/with-color green
      (ui/rectangle world-width 10))]))

(defn player-feet-level []
  (let [player-y     (:player-y @state)
        player-value (ui/translate 0 player-y (player))
        [_ height]   (ui/bounds player-value)
        [_ y]        (ui/origin player-value)]
    (+ height y)))

(defn move [{:keys [player-y-speed player-x-speed]}]
  (swap! state update :player-y + player-y-speed)
  (swap! state update :player-x + player-x-speed))

(defn fall [{:keys [ground-level]}]
  (if (< (player-feet-level) ground-level)
    (swap! state update :player-y-speed inc)
    (swap! state assoc :player-y-speed 0)))

(defn stop [{:keys [player-x-speed]}]
  (swap! state update :player-x-speed
         (cond (< player-x-speed 0) inc
               (> player-x-speed 0) dec
               :else                identity)))

(defn info [{:keys [frame-number mouse-position] :as world-state}]
  (ui/vertical-layout
   (ui/on :mouse-move-global
          (fn [[x y]] (swap! state assoc :mouse-position [x y]))
          (ui/label (str "mouse" mouse-position)))
   (ui/label frame-number)
   (ui/label (:player-x-speed world-state))))

(defn world [{:keys [player-y player-x world-width ground-level]
              :as   world-state}]
  [#_#_#_#_(sky world-width ground-level)
         (trees world-state)
       (ground world-state)
     (ui/translate player-x player-y (player))
   #_(info world-state)])

(def window (java2d/run #(world @state)
                        {:window-title "jump!"}))

(defn on-frame []
  (stop @state)
  (move @state)
  (fall @state)
  (let [now                              (time/now)
        {:keys [frame-number last-time]} @state
        dt                               (time/micros (time/between last-time now))]
    (swap! state assoc
           :frame-number (inc frame-number)
           :fps (int (/ 1000000 dt))
           :last-time now
           :dt (float (/ dt 1000000)))))

(go-loop []
  (<! (timeout 1000/60))
  (on-frame)
  ((::java2d/repaint window))
  (recur))
